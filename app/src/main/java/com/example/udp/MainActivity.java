package com.example.udp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

public class MainActivity extends AppCompatActivity {

    public class SendJob extends AsyncTask<String,String,String> {

        private MainActivity _main;
        public SendJob(MainActivity main) {
            super();
            _main = main;
        }
        @Override
        protected String doInBackground(String...value) {
            String  res  = "";
            //ToDo
            try {
                send();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //
        }

        @Override
        protected void onPostExecute(String result) {
        }
    }

    public class AsyncJob extends AsyncTask<String,String,String> {

        private MainActivity _main;
        public AsyncJob(MainActivity main) {
            super();
            _main = main;
        }
        @Override
        protected String doInBackground(String...value) {
            String  res  = "";
            //ToDo
            try {
                receive();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //
        }

        @Override
        protected void onPostExecute(String result) {
        }
    }

    MulticastSocket s = null;
    InetAddress group = null;
    NetworkInterface nif = null;
    int PORT = 6769;
    void send() throws IOException {
        // join a Multicast group and send the group salutations
        group = InetAddress.getByName("228.5.6.7");
        s = new MulticastSocket(PORT);

        Enumeration enumeration = NetworkInterface.getNetworkInterfaces();
        NetworkInterface eth0 = null;
        while (enumeration.hasMoreElements()) {
            eth0 = (NetworkInterface) enumeration.nextElement();
            //there is probably a better way to find ethernet interface
            if (eth0.getName().equals("eth0")) {
                break;
            }
        }
        Log.d("udp", "eth0:"+eth0);
        s.setNetworkInterface(eth0);
        s.setSoTimeout(10000);
        s.joinGroup(new InetSocketAddress(group, PORT), eth0);
        if(s != null) {
            String msg = "Hello";
            byte[] bytes = msg.getBytes(StandardCharsets.UTF_8);
            DatagramPacket hi = new DatagramPacket(bytes, bytes.length,
                    group, PORT);
            s.send(hi);
        }
    }

    void sendTask() {
        //非同期タスクの生成
        final SendJob asynctask = new SendJob(this);
        //実行
        asynctask.execute("A","B","C");
    }
    void receiveTask() {
        //非同期タスクの生成
        final AsyncJob asynctask = new AsyncJob(this);
        //実行
        asynctask.execute("A","B","C");
    }

    void receive() throws IOException {
        String msg = "Hello";
        group = InetAddress.getByName("228.5.6.7");
        s = new MulticastSocket(PORT);
        s.setSoTimeout(10000);


        Enumeration enumeration = NetworkInterface.getNetworkInterfaces();
        NetworkInterface eth0 = null;
        while (enumeration.hasMoreElements()) {
            eth0 = (NetworkInterface) enumeration.nextElement();
            //there is probably a better way to find ethernet interface
            if (eth0.getName().equals("eth0")) {
                break;
            }
        }
        Log.d("udp", "eth0:"+eth0);
        s.setNetworkInterface(eth0);
        s.joinGroup(new InetSocketAddress(group, PORT), eth0);
        byte[] bytes = msg.getBytes(StandardCharsets.UTF_8);
        DatagramPacket hi = new DatagramPacket(bytes, bytes.length,
                group, PORT);
        for(int i = 0; i < 10; ++i) {
            if (s != null) {
                s.receive(hi);

                TextView tv = findViewById(R.id.textView);
                tv.setText("receive:" + hi.getLength());
            }
        }

        if(s != null) {
            s.leaveGroup(group);
            s = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WifiManager wm = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        WifiManager.MulticastLock multicastLock = wm.createMulticastLock("mydebuginfo");
        multicastLock.setReferenceCounted(true);
        multicastLock.acquire();

        Button btn_send = findViewById(R.id.button_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTask();
            }
        });

        final Button btn_receive = findViewById(R.id.button_receive);
        btn_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn_receive.getText().equals("RECEIVE")) {
                    btn_receive.setText("RECEIVING");
                    receiveTask();
                }
                else {
                    btn_receive.setText("RECEIVE");
                }
            }
        });
    }
}